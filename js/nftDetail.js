
const Web3Modal = window.Web3Modal.default;
const WalletConnectProvider = window.WalletConnectProvider.default;
const Fortmatic = window.Fortmatic;
const evmChains = window.evmChains;

// Web3modal instance
let web3Modal
let provider;
let connectedWallet;
let web3;
let nftContractAddress = "0x6575d803493bd4AeE644ff31aa0F666BFe5731dF";
let nftContract;

let dutchAuctionContractAddress = "0xdc3ba16B514F599a6Cd8240d07346b5AC8c2055C";
let dutchAuctionContract;

let realEstateErc20TokenContractAddress = "0x6BC7474B2ACB2A4d32a4592eace063B41b738320";
let realEstateErc20TokenContract;

let urlString = window.location.href; // www.test.com?filename=test
let url = new URL(urlString);
let image = url.searchParams.get("img");
document.getElementById("nftUrl").src = image;
let tokenId = url.searchParams.get("tokenId");

let nftOwner;
let auctionCounter;
let isListed = 0;

async function initilize() {


    const providerOptions = {
        walletconnect: {
            package: WalletConnectProvider,
            options: {
                infuraId: "f790522edd494383a9a3ffc023c6c86f",
            }
        },

    };

    web3Modal = new Web3Modal({
        cacheProvider: true,
        providerOptions,
        disableInjectedProvider: false,
    });

    if (web3Modal.cachedProvider) {
        try {
            provider = await web3Modal.connect();
            web3 = new Web3(provider);
        } catch (e) {
            console.log("Could not get a wallet connection", e);
            return;
        }

        // // Subscribe to accounts change
        provider.on("accountsChanged", async (accounts) => {
            // accounts = await web3.eth.getAccounts();
            connectedWallet = accounts.toString();
            await checkOwner();

        });

        // Subscribe to chainId change
        provider.on("chainChanged", (chainId) => {
            // fetchAccountData();
        });

    }


    if (provider) {

        let collectionAbi;
        let dutchAuctionAbi;
        let realEstateErc20TokenAbi;
        let accounts = await web3.eth.getAccounts();
        connectedWallet = accounts[0];
        await $.getJSON("./abi/nftContractAbi.json", function (abi) { collectionAbi = abi; });
        await $.getJSON("./abi/dutchAuctionContractAbi.json", function (abi) { dutchAuctionAbi = abi; });
        await $.getJSON("./abi/realEstateErc20TokenAbi.json", function (abi) { realEstateErc20TokenAbi = abi; });



        nftContract = new web3.eth.Contract(collectionAbi, nftContractAddress);
        dutchAuctionContract = new web3.eth.Contract(dutchAuctionAbi, dutchAuctionContractAddress);
        realEstateErc20TokenContract = new web3.eth.Contract(realEstateErc20TokenAbi, realEstateErc20TokenContractAddress);

    }

    await checkOwner();

    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "linear",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
}

async function checkOwner() {
    nftOwner = await nftContract.methods.ownerOf(tokenId).call();
    auctionCounter = await dutchAuctionContract.methods.auctionCounter().call();

}

async function ListNFtForAuction() {

    if (nftOwner.toLowerCase() == connectedWallet.toLowerCase()) {

        if (!isListed) {
            if (confirm("For list nft for auction, I am not create UI form for this,I passed following values to smart contract") == true) {
                if (confirm("startPrice:50000000000000000000,\n endPrice:40000000000000000000,\n duration:1800,\n nftContractAddress:0x6575d803493bd4AeE644ff31aa0F666BFe5731dF,\n tokenId:(tokenIdListNft)") == true) {

                    document.getElementById("ring").style.display = "block";

                    let estimatedGas = await dutchAuctionContract.methods.listNftForAuction("50000000000000000000", "40000000000000000000", "1800", "0x6575d803493bd4AeE644ff31aa0F666BFe5731dF", tokenId)
                        .estimateGas({
                            from: connectedWallet
                        });


                    await dutchAuctionContract.methods.listNftForAuction("50000000000000000000", "40000000000000000000", "1800", "0x6575d803493bd4AeE644ff31aa0F666BFe5731dF", tokenId)
                        .send({
                            from: connectedWallet,
                            gasPrice: estimatedGas
                        })
                        .on("error", (error, receipt) => {
                            document.getElementById("ring").style.display = "none";
                            toastr.error("error or canceled");
                        })
                        .on("transactionHash", (txHash) => {

                            console.log(txHash);

                        })
                        .then((tx) => {
                            console.log("Listing Transaction: ", tx);
                            document.getElementById("ring").style.display = "none";
                            toastr.info("NFt listed for Auction successfully");

                        })
                        .catch((err) => {
                            console.log(err);
                            document.getElementById("ring").style.display = "none";
                            toastr.error("error or canceled");
                        });
                }
            }
        } else {
            toastr.info("NFT already listed for auction.");
        }


    } else {
        toastr.info("You are not NFT owner\n, Only NFT owner can list their NFT for auction.");
    }

}

async function placeBid() {

    console.log(realEstateErc20TokenContract);

    if (nftOwner.toLowerCase() != connectedWallet.toLowerCase()) {

        let auctionCounter = await dutchAuctionContract.methods.auctionCounter().call();

        let isCheck = 0;
        let auctions;
        let auctionId;
        for (let i = 1; i < auctionCounter; i++) {

            auctions = await dutchAuctionContract.methods.auctions(i).call();
            if (auctions.tokenId === tokenId) {
                isCheck = 1;
                auctionId = i;
            }
        }

        if (isCheck) {

            if (confirm("For bid nft for auction, I am not create UI form for this,I passed following values to smart contract") == true) {
                if (confirm("auctionId:" + auctionId + "\n amount:49000000000000000000") == true) {

                    let balance = await realEstateErc20TokenContract.methods.balanceOf(connectedWallet).call();

                    if (balance >= Number(49000000000000000000)) {

                        let approveEstimatedGas = await realEstateErc20TokenContract.methods.approve(dutchAuctionContractAddress, "49000000000000000000").estimateGas({
                            from: connectedWallet
                        });
                        document.getElementById("ring").style.display = "block";

                        realEstateErc20TokenContract.methods.approve(dutchAuctionContractAddress, "49000000000000000000")
                            .send({ from: connectedWallet, gasPrice: approveEstimatedGas })
                            .then(() => {

                                dutchAuctionContract.methods.placeBid(auctionId, "49000000000000000000")
                                    .estimateGas({
                                        from: connectedWallet
                                    })
                                    .then(async (placeBidestimatedGas) => {

                                        dutchAuctionContract.methods.placeBid(auctionId, "49000000000000000000")
                                            .send({
                                                from: connectedWallet,
                                                gasPrice: placeBidestimatedGas
                                            }).on("error", (error, receipt) => {
                                                document.getElementById("ring").style.display = "none";
                                                toastr.error("error or canceled");
                                            })
                                            .on("transactionHash", (txHash) => {
                                                console.log(txHash);
                                            })
                                            .then((tx) => {
                                                console.log("bidded Transaction: ", tx);
                                                document.getElementById("ring").style.display = "none";
                                                toastr.info("Bidded on NFT successfully.");

                                            })
                                            .catch((err) => {
                                                console.log(err);
                                                document.getElementById("ring").style.display = "none";
                                                toastr.error(err);
                                            });

                                    })
                                    .catch((err) => {
                                        console.log("Error ", err);
                                        document.getElementById("ring").style.display = "none";
                                        toastr.error(err.toString().split("\n{")[0]);
                                    });
                            })
                            .catch((error) => {
                                document.getElementById("ring").style.display = "none";
                                toastr.error('Error Or Cancelled');
                            });


                    } else {
                        toastr.error("Insufficient erc20 token");
                    }
                }
            }
        } else {
            toastr.info("You can not bid, because nft not listed for auction yet.")
        }

    } else {
        toastr.info("NFT owner can not bid on their NFT.");
    }

}

async function getAuctionDetails() {

    for (let i = 1; i < auctionCounter; i++) {
        let auctions = await dutchAuctionContract.methods.auctions(i).call();
        if (auctions.tokenId === tokenId) {
            isListed = 1;
            await getAllDetails(auctions);
            break;
        }
    }
}
async function getAllDetails(auctions) {

    // console.log(auctions);
    document.getElementById("bidDetailDiv").style.display = "block";

    let startPrice = await web3.utils.fromWei(auctions.startPrice, 'ether');
    let endPrice = await web3.utils.fromWei(auctions.endPrice, 'ether');


    document.getElementById("seller").innerText = auctions.seller;
    document.getElementById("highestBidder").innerText = auctions.highestBidder;
    document.getElementById("startPrice").innerText = startPrice + " Real Token";
    document.getElementById("endPrice").innerText = endPrice + " Real Token";
    document.getElementById("startTime").innerText = auctions.startTime;
    document.getElementById("endTime").innerText = auctions.endTime;
    document.getElementById("auctionEnded").innerText = auctions.ended;
    document.getElementById("realEstateNftContractAddress").innerText = auctions.realEstateNftContractAddress;
    document.getElementById("tokenId").innerText = auctions.tokenId;

}

document.getElementById("ring").style.display = "none";
window.addEventListener('load', async () => {
    await initilize();
    await getAuctionDetails();
});