
const Web3Modal = window.Web3Modal.default;
const WalletConnectProvider = window.WalletConnectProvider.default;
const Fortmatic = window.Fortmatic;
const evmChains = window.evmChains;

// Web3modal instance
let web3Modal
let provider;
let connectedWallet;
// let nftContractAddress = "0x716842825b879f93bc564F1d7439a953F0F72fF9";
let nftContractAddress = "0x6575d803493bd4AeE644ff31aa0F666BFe5731dF";
let tokenDataArray = [];
let nftMetadataArray = [];
// let accounts;

async function initilize() {

    console.log("Initializing example");

    const providerOptions = {
        walletconnect: {
            package: WalletConnectProvider,
            options: {
                infuraId: "f790522edd494383a9a3ffc023c6c86f",
            }
        },

    };

    web3Modal = new Web3Modal({
        cacheProvider: true, // optional
        providerOptions, // required
        disableInjectedProvider: false, // optional. For MetaMask / Brave / Opera.
    });

    if (web3Modal.cachedProvider) {

        try {
            provider = await web3Modal.connect();
        } catch (e) {
            console.log("Could not get a wallet connection", e);
            return;
        }

        // // Subscribe to accounts change
        provider.on("accountsChanged", (accounts) => {
            // fetchAccountData();
        });

        // Subscribe to chainId change
        provider.on("chainChanged", (chainId) => {
            // fetchAccountData();
        });

        // Subscribe to networkId change
        provider.on("networkChanged", (networkId) => {
            // fetchAccountData();
        });

        // await refreshAccountData();
    }

    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "linear",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
}

async function fetchAccountData() {

    const web3 = new Web3(provider);
    const chainId = await web3.eth.getChainId();
    const chainData = evmChains.getChain(chainId);

    if (chainId != 5) {
        toastr.info("Please connect to goerli test network");
        await onDisconnect();
        return;
    }

    document.getElementById("ring").style.display = "block";
    let accounts = await web3.eth.getAccounts();

    // MetaMask does not give you all accounts, only the selected account

    document.getElementById("accountDetails").style.display = "block";
    // document.querySelector("#btn-disconnect").style.display = "none";

    connectedWallet = accounts[0];
    let balanceWei = await web3.eth.getBalance(connectedWallet);
    let balance = web3.utils.fromWei(balanceWei, "ether");

    document.getElementById("chainName").innerHTML = chainData.name;
    document.getElementById("balance").innerHTML = balance;


    let imgDiv = document.getElementById('nftImgDiv');
    imgDiv.innerHTML = '';
    await getNFTs();
    await getNftDetails();
    console.log(tokenDataArray);

    for (let i = 0; i < tokenDataArray.length; i++) {
        let anchor = document.createElement("a");
        // anchor.href="/nftDetail.html?img=https://ipfs.io/ipfs/QmPPGAxG6nvrPa1MrPnegjXHFXdKkWopWNaUYBdetNJ5fx&tokenId="+i;
        anchor.href = "/nftDetail.html?img=" + tokenDataArray[i].tokenURI + "&tokenId=" + tokenDataArray[i].tokenId;
        let img = document.createElement("img");
        // img.src = "https://ipfs.io/ipfs/QmPPGAxG6nvrPa1MrPnegjXHFXdKkWopWNaUYBdetNJ5fx";
        img.src = tokenDataArray[i].tokenURI;
        img.className = "imgStyle";
        anchor.append(img);
        imgDiv.append(anchor);
    }

    document.getElementById("ring").style.display = "none";
    document.getElementById("nftImgDiv").style.display = "block";

}

async function isMetaMaskConnected() {
    return (accounts && accounts.length > 0);
}

async function refreshAccountData() {

    await fetchAccountData();
}

async function onConnect() {

    console.log("Opening a dialog", web3Modal);
    try {
        provider = await web3Modal.connect();
        document.getElementById("cnctBtn").innerHTML = "Disconnect";
    } catch (e) {
        console.log("Could not get a wallet connection", e);
        return;
    }

    // Subscribe to accounts change
    provider.on("accountsChanged", (accounts) => {
        fetchAccountData();
    });

    // Subscribe to chainId change
    provider.on("chainChanged", (chainId) => {
        fetchAccountData();
    });

    // Subscribe to networkId change
    provider.on("networkChanged", (networkId) => {
        fetchAccountData();
    });

    await refreshAccountData();
}

async function onDisconnect() {


    if (provider.close) {
        await provider.close();

        await web3Modal.clearCachedProvider();
        provider = null;
    }
    await web3Modal.clearCachedProvider();
    provider = null;
    connectedWallet = null;
    document.getElementById("accountDetails").style.display = "none";
    document.getElementById("cnctBtn").innerHTML = "Connect";
    document.getElementById("ring").style.display = "none";
    document.getElementById("nftImgDiv").style.display = "none";
    tokenDataArray = [];
    nftMetadataArray = [];

}

async function metamask() {
    let text = document.getElementById("cnctBtn").innerHTML;
    if (text === "Connect") {
        onConnect();
    } else if (text === "Disconnect") {
        onDisconnect();
    }

}

async function getNFTs() {

    const web3 = new Web3(provider);
    let collectionAbi;
    await $.getJSON("./abi/nftContractAbi.json", function (abi) { collectionAbi = abi; });

    // console.log("collectionAbi ", collectionAbi);

    let collectionContract = new web3.eth.Contract(collectionAbi, nftContractAddress);
    let totalSupply = await collectionContract.methods.totalSupply().call();
    console.log('totalSupply ', totalSupply);
    
    for (var i = 0; i < totalSupply; i++) {
        let tokenId = await collectionContract.methods.tokenByIndex(i).call();
        let tokenURI = await collectionContract.methods.tokenURI(tokenId).call();

        let metaDataJson = {};
        metaDataJson.tokenId = tokenId;
        metaDataJson.tokenURI = tokenURI;
        nftMetadataArray.push(metaDataJson);

    }
}

async function getNftDetails() {

    for (var i = 0; i < nftMetadataArray.length; i++) {

        let yourMetadata = await getMetadata(nftMetadataArray[i]['tokenURI'], nftMetadataArray[i]['tokenId']).then(async function (res) {
            return res;
        }).catch(async function (message) {
            return false;
        });

        let tokenJson = {};
        tokenJson['tokenId'] = nftMetadataArray[i]['tokenId'];
        tokenJson['tokenURI'] = yourMetadata.image;
        tokenDataArray.push(tokenJson);
    }
}

async function getMetadata(tokenURI, tokenId) {
    return jQuery.ajax({
        type: 'GET',
        dataType: "json",
        url: tokenURI,
        // headers:{ 'Authorization' : 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBWZXIiOiIwLjAuMCIsImV4cCI6NDcyNjM4OTEyMiwibG9jYWxlIjoiIiwibWFzdGVyVmVyIjoiIiwicGxhdGZvcm0iOiIiLCJwbGF0Zm9ybVZlciI6IiIsInVzZXJJZCI6IiJ9.QIZbmB5_9Xlap_gDhjETfMI6EAmR15yBtIQkWFWJkrg',},
        success: function (data, status, xhr) {
            return data;
        }
    });
}

document.getElementById("nftImgDiv").style.display = "none";
window.addEventListener('load', async () => {
    initilize();
    document.getElementById("ring").style.display = "none";

});